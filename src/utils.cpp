#include "utils.h"

#include <sstream>
#include <filesystem>

#include "log.h"

namespace mdk {
    bool writeFileContent(const std::string& filename, const char* data, unsigned int size)
    {
        std::ofstream ofs(filename, std::ios_base::out);
        if (ofs) {
            ofs.write(data, size);
            ofs.close();
            return true;
        }
        return false;
    }

    char* readStreamContent(std::ifstream& ifs)
    {
        std::streampos begin = ifs.tellg();
        ifs.seekg (0, std::ios::end);
        std::streampos end = ifs.tellg();
        ifs.seekg (0, std::ios::beg);
        unsigned int size = end - begin;
        if (size == 0)
            return nullptr;

        char* data = new char[size];
        ifs.read(data, size);
        ifs.close();
        return data;
    }

    char* readFileContent(const std::string& filename)
    {
        std::ifstream ifs(filename, std::ios::in);
        return readStreamContent(ifs);
    }

    std::string getStreamContent(std::ifstream& ifs)
    {
        if (!ifs)
            throw(errno);

        std::ostringstream contents;
        contents << ifs.rdbuf();
        ifs.close();
        return contents.str();
    }

    std::string getFileContent(const std::string& filename)
    {
        std::ifstream ifs(filename, std::ios::in);
        return getStreamContent(ifs);
    }

    void getStreamLines(std::ifstream& ifs, std::set<std::string>& vect)
    {
        vect.clear();
        char buffer[256];
        while (ifs.getline(buffer, 256)) {
            vect.emplace(buffer);
        }
    }

    void getFileLines(const std::string& filename, std::set<std::string>& vect)
    {
        std::ifstream ifs(filename, std::ios::in);
        getStreamLines(ifs, vect);
        ifs.close();
    }


    bool isDirectory(const std::string& directory)
    {
        std::filesystem::directory_entry entry(directory);
        if (!entry.exists()) {
            Log(libecap::ilCritical) << "Path " << entry.path() << " does not exists";
            return false;
        }

        if (!entry.is_directory()) {
            Log(libecap::ilCritical) << "Path " << entry.path() << " is not a directory";
            return false;
        }

        return true;
    }

    bool isFile(const std::string& file)
    {
        std::filesystem::directory_entry entry(file);
        if (!entry.exists()) {
            Log(libecap::ilCritical) << "Path " << entry.path() << " does not exists";
            return false;
        }

        if (!entry.is_regular_file()) {
            Log(libecap::ilCritical) << "Path " << entry.path() << " is not a regular file";
            return false;
        }

        return true;
    }

    void addDirectoryFile(
        std::list<std::string>& list,
        const std::string& file)
    {
        if (!isFile(file)) return;

        Log(libecap::ilDebug) << "Adding file " << file;
        list.push_back(file);
    }

    void addDirectoryFiles(
        std::list<std::string>& list,
        const std::string& directory)
    {
        if (!isDirectory(directory)) return;

        std::filesystem::directory_iterator dir(directory);
        for (auto& file : dir)
            addDirectoryFile(list, file.path());
    }

    void addCategories(
        std::list<std::string>& list,
        const std::string& categories)
    {
        char buffer[256];
        std::stringstream sstr(categories);
        while (sstr.good()) {
            sstr.getline(buffer, 256, ',');
            list.emplace_back(buffer);
        }
    }
}