/*                                    HTParse.c
**  URI MANAGEMENT
**
**  (c) COPYRIGHT MIT 1995.
**  Please first read the full copyright statement in the file COPYRIGH.
**  @(#) $Id: HTParse.c,v 2.80 1999/02/22 22:10:11 frystyk Exp $
**
** history:
**  May 12 94   TAB added as legal char in HTCleanTelnetString
**
*/

/* Library include files */
#include "HTParse.h"

#include <stdexcept>

HTURI::HTURI(const std::string &uri)
: uri(uri)
{
    size_t pos = uri.find(':');
    if (pos == std::string::npos) {
        throw std::invalid_argument("missing scheme");
    }
    scheme = std::string_view(uri.c_str(), pos);
    pos++; // consume ':' char

    if (uri.size() >= pos+2 && uri[pos] == '/' && uri[pos+1] == '/') { // authority
        pos += 2; // consume '//' chars
        size_t authority_end = uri.find('/', pos);
        authority = (authority_end == std::string::npos) ?
            std::string_view(uri.c_str()+pos) :
            std::string_view(uri.c_str()+pos, authority_end-pos);

        size_t userinfo_end = uri.find('@', pos);
        if (userinfo_end != std::string::npos && userinfo_end < authority_end) { // userinfo
            userinfo = std::string_view(uri.c_str()+pos, userinfo_end-pos);
            size_t username_end = uri.find(':', pos);
            if (username_end != std::string::npos && username_end < userinfo_end) { //username
                username = std::string_view(uri.c_str()+pos, username_end-pos);
                pos = username_end+1; // consume username and ':' char
                password = std::string_view(uri.c_str()+pos, userinfo_end-pos);
            } else {
                username = userinfo;
            }
            pos = userinfo_end+1; // consume userinfo and '@' char
        }

        if (uri[pos] == '[') { // IPv6 host
            pos++; // Consume '[' char
            size_t host_end = uri.find(']', pos);
            if (host_end == std::string::npos) {
                throw std::invalid_argument("missing closing bracket");
            }
            host = std::string_view(uri.c_str()+pos, host_end-pos);
            pos = host_end+1; // consume host and ']' char
        } else {
            size_t host_end = std::min(uri.find(':', pos), authority_end);
            host = (host_end == std::string::npos) ?
                std::string_view(uri.c_str()+pos) :
                std::string_view(uri.c_str()+pos, host_end-pos);
            pos = host_end; // consume host
        }
        if (host.empty()) {
            throw std::invalid_argument("missing host");
        }

        if (pos == std::string::npos) return;
        if (pos < authority_end) {
            if (uri[pos] != ':') {
                throw std::invalid_argument("missing port colon");
            }
            pos++; // Consume ':' char
            port = (authority_end == std::string::npos) ?
                std::string_view(uri.c_str()+pos) :
                std::string_view(uri.c_str()+pos, authority_end-pos);
            pos = authority_end; // consume port
        }
    }

    if (pos == std::string::npos) return;
    size_t path_end = std::min(uri.find('?', pos), uri.find('#', pos));
    path = (path_end == std::string::npos) ?
        std::string_view(uri.c_str()+pos) :
        std::string_view(uri.c_str()+pos, path_end-pos);
    pos = path_end; // consume path

    if (pos == std::string::npos) return;
    if (uri.size() >= pos+1 && uri[pos] == '?') { // query
        pos++; // consume '?' char
        size_t query_end = uri.find('#', pos);
        query = (query_end == std::string::npos) ?
            std::string_view(uri.c_str()+pos) :
            std::string_view(uri.c_str()+pos, query_end-pos);
        pos = query_end;
    }

    if (pos == std::string::npos) return;
    if (uri.size() >= pos+1 && uri[pos] == '#') { // fragment
        pos++; // consume '#' char
        fragment = std::string_view(uri.c_str()+pos);
    }
}
