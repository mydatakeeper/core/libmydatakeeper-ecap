/*
**  (c) COPYRIGHT MIT 1995.
**  Please first read the full copyright statement in the file COPYRIGH.
*/

#ifndef HTPARSE_H
#define HTPARSE_H

#include <string>
#include <string_view>

struct HTURI {
    HTURI(const std::string& uri);

    const std::string &uri;

    std::string_view scheme;
    std::string_view authority;
    std::string_view userinfo;
    std::string_view username;
    std::string_view password;
    std::string_view host;
    std::string_view port;
    std::string_view path;
    std::string_view query;
    std::string_view fragment;
};

#endif  /* HTPARSE_H */