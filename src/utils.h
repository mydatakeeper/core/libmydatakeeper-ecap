#ifndef UTILS_H_INCLUDED
#define UTILS_H_INCLUDED

#include <string>
#include <fstream>
#include <list>
#include <set>

namespace mdk {

    bool isDirectory(const std::string& directory);
    bool isFile(const std::string& file);

    void addDirectoryFile(
        std::list<std::string>& list,
        const std::string& file);
    void addDirectoryFiles(
        std::list<std::string>& list,
        const std::string& directory);
    void addCategories(
        std::list<std::string>& list,
        const std::string& categories);

    bool writeFileContent(const std::string& filename, const char* data, unsigned int size);

    char* readStreamContent(std::ifstream& ifs);
    char* readFileContent(const std::string& filename);
    std::string getStreamContent(std::ifstream& ifs);
    std::string getFileContent(const std::string& filename);
    void getStreamLines(std::ifstream& ifs, std::set<std::string>& vect);
    void getFileLines(const std::string& filename, std::set<std::string>& vect);

} /* namespace mdk */

#endif /* UTILS_H_INCLUDED */
