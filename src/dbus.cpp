#include "dbus.h"

#include <dbus-c++/dbus.h>

#include "dbus/filter-proxy.h"
#include "dbus/squid-adaptor.h"

#include "log.h"

extern const string FilterPath("/fr/mydatakeeper/filter");
extern const string FilterName("fr.mydatakeeper.filter");

extern const string SquidPath("/org/squid_cache/proxy");
extern const string SquidName("org.squid_cache.proxy");

namespace {
DBus::BusDispatcher dispatcher;
bool init = false;

void initDbus()
{
    if (!init) {
        // Setup DBus dispatcher
        DBus::default_dispatcher = &dispatcher;
        init = true;
    }
}

} // end of anonymous namespace

DbusAdaptor::DbusAdaptor(const std::string& bus)
{
    initDbus();

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    Log() << "Requesting DBus name " << SquidName;
    conn.request_name(SquidName.c_str());

    Log(libecap::ilDebug) << "Initializing DBus FilterProxy";
    proxy = new FilterProxy(conn);

    Log(libecap::ilDebug) << "Initializing DBus SquidAdaptor";
    adaptor = new SquidAdaptor(conn);
}

DbusAdaptor::~DbusAdaptor()
{
    if (thread_running.load())
        stopSignalThread();

    delete proxy;
    delete adaptor;
}

bool DbusAdaptor::filter(
    const std::string& src,
    const std::string& url, 
    const std::string& domain,
    const std::string& refererDomain,
    const std::string& type)
{
    std::map<std::string, std::string> metadata = {};
    if (!refererDomain.empty()) {
        Log(libecap::ilDebug)
            << "Filtering URL " << url
            << " with domain " << domain
            << " and referer domain " << refererDomain;
        metadata["REFERER"] = refererDomain;
    } else {
        Log(libecap::ilDebug)
            << "Ad blocker: Filtering URL " << url
            << " with domain " << domain;
    }

    const bool ret = proxy->filter_url(url, refererDomain.empty() ? domain : refererDomain, type);
    if (ret) {
        metadata["BLOCKED"] = true;
    }

    signals.emplace(src, domain, url, metadata);
    cv_signal.notify_one();


    return ret;
}

void DbusAdaptor::runSignalThread()
{
    thread_running.store(true);
    thread_signal = std::thread([this]() { signalRunner(); });
}

void DbusAdaptor::stopSignalThread()
{
    {
        std::unique_lock<std::mutex> lck(mtx_signal);
        thread_running.store(false);
        cv_signal.notify_one();
    }

    thread_signal.join();
}

void DbusAdaptor::signalRunner()
{
    while (thread_running.load()) {
        if (signals.empty()) {
            std::unique_lock<std::mutex> lck(mtx_signal);
            cv_signal.wait(lck);
        } else {
            const auto& sig = signals.front();
            adaptor->RequestUrl(std::get<0>(sig), std::get<1>(sig), std::get<2>(sig), std::get<3>(sig));
            signals.pop();
        }
    }
}
