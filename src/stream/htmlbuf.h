#ifndef HTMLBUF_H_INCLUDED
#define HTMLBUF_H_INCLUDED

#include <functional>
#include <map>
#include <sstream>
#include <stack>
#include <streambuf>
#include <string>
#include <tuple>

namespace html {

enum class tag_type : uint8_t {
    none  = 0,
    start = 1 << 0,
    end   = 1 << 1,
    empty = 1 << 2,
    any   = start | end | empty,
};  // enum class tag_type

inline bool operator! (tag_type a) { return a == tag_type::none; }
inline tag_type operator~ (tag_type a) { return (tag_type)~(int)a; }
inline tag_type operator| (tag_type a, tag_type b) { return (tag_type)((int)a | (int)b); }
inline tag_type operator& (tag_type a, tag_type b) { return (tag_type)((int)a & (int)b); }
inline tag_type operator^ (tag_type a, tag_type b) { return (tag_type)((int)a ^ (int)b); }
inline tag_type& operator|= (tag_type& a, tag_type b) { return (tag_type&)((int&)a |= (int)b); }
inline tag_type& operator&= (tag_type& a, tag_type b) { return (tag_type&)((int&)a &= (int)b); }
inline tag_type& operator^= (tag_type& a, tag_type b) { return (tag_type&)((int&)a ^= (int)b); }

class streambuf : public std::streambuf
{
public:
    streambuf(std::streambuf *sbuf_p)
    :   sbuf_p(sbuf_p)
    ,   reached_eof(false)
    ,   before_it(before.begin())
    ,   instead_it(instead.begin())
    ,   after_it(after.begin())
    {}

    using tag_name = std::string;
    using attribute_name = std::string;
    using attribute_value = std::string;
    using attribute = std::pair<attribute_name, attribute_value>;
    using attributes = std::map<attribute_name, attribute_value>;

    using callback_key = std::tuple<tag_name, tag_type>;
    using callback_return = std::tuple<std::string, std::string, std::string>;
    using callback_type = std::function<callback_return(tag_type type, const tag_name&, const attributes&)>;
    using callback_void_type = std::function<void(tag_type type, const tag_name&, const attributes&)>;

    void set_callback(callback_type func, const tag_name& tag = "*", tag_type type = tag_type::any)
    {
        if (!!(type & tag_type::start))
            callbacks[std::make_tuple(tag, tag_type::start)] = func;

        if (!!(type & tag_type::end))
            callbacks[std::make_tuple(tag, tag_type::end)] = func;

        if (!!(type & tag_type::empty))
            callbacks[std::make_tuple(tag, tag_type::empty)] = func;
    }

    void unset_callback(const tag_name& tag = "*", tag_type type = tag_type::any)
    {
        if (!!(type & tag_type::start)) {
            auto it = callbacks.find(std::make_tuple(tag, tag_type::start));
            if (it != callbacks.end())
                callbacks.erase(it);
        }

        if (!!(type & tag_type::end)) {
            auto it = callbacks.find(std::make_tuple(tag, tag_type::end));
            if (it != callbacks.end())
                callbacks.erase(it);
        }

        if (!!(type & tag_type::empty)) {
            auto it = callbacks.find(std::make_tuple(tag, tag_type::empty));
            if (it != callbacks.end())
                callbacks.erase(it);
        }
    }


    virtual std::streamsize xsputn (const std::streambuf::char_type* s, std::streamsize n)
    {
        reached_eof = false;
        return sbuf_p->sputn(s, n);
    }

    virtual std::streambuf::int_type overflow (std::streambuf::int_type c = EOF)
    {
        reached_eof = false;
        return sbuf_p->sputc(c);
    }


    virtual std::streamsize xsgetn (std::streambuf::char_type *s, std::streamsize n)
    {
        std::streamsize i = 0;
        std::streampos pos = sbuf_p->pubseekoff(0, std::ios_base::cur, std::ios_base::in);
        for (; i < n; ++i) {
            std::streambuf::int_type ret = _uflow(pos);
            if (ret == traits_type::eof()) {
                s[i] = '\0';
                break;
            } else {
                s[i] = traits_type::to_char_type(ret);
            }
        }
        return i;
    }

    virtual std::streambuf::int_type uflow()
    {
        auto pos = sbuf_p->pubseekoff(0, std::ios_base::cur, std::ios_base::in);
        return _uflow(pos);
    }
    std::streambuf::int_type _uflow(std::streampos& pos)
    {
        auto ret = _underflow(pos);
        if (ret != traits_type::eof() && !buffer_read()) {
            sbuf_p->sbumpc();
            pos += 1;
        }
        return ret;
    }

    virtual std::streambuf::int_type underflow()
    {
        auto pos = sbuf_p->pubseekoff(0, std::ios_base::cur, std::ios_base::in);
        return _underflow(pos);
    }
    std::streambuf::int_type _underflow(std::streampos& pos)
    {
        auto ret = buffer_peek();
        if (ret != traits_type::eof())
            return ret;

        if (reached_eof)
            return traits_type::eof();

        std::string content;
        tag_type type;
        tag_name name;
        attributes attributes;
        try {
            if (try_read_tag(type, name, attributes, content, pos)) {
                setup_buffer(type, name, attributes, content);
                return buffer_peek();
            }
            return traits_type::to_int_type(peek());
        } catch (std::streambuf::int_type c) {
            reached_eof = true;
            std::streampos first_position = -1;
            while (!positions.empty()) {
                first_position = positions.top();
                positions.pop();
            }
            if (first_position != -1 && first_position != pos) {
                sbuf_p->pubseekpos(first_position);
            }
            return c;
        }
    }

private:
    /* Wrapped std::streambuf */
    std::streambuf *sbuf_p;
    bool reached_eof;

    /* Callbacks */
    std::map<callback_key, callback_type> callbacks;

    /* Replacement values / iterators */
    std::string before, instead, after;
    std::string::const_iterator before_it, instead_it, after_it;

    void setup_buffer(
        tag_type type,
        const tag_name& name,
        const attributes& attributes,
        const std::string& content)
    {
        auto it = callbacks.find(std::make_tuple(name, type));
        if (it == callbacks.end())
            it = callbacks.find(std::make_tuple("*", type));
        if (it != callbacks.end()) {
            auto replacement = it->second(type, name, attributes);
            before = std::get<0>(replacement);
            instead = std::get<1>(replacement);
            after = std::get<2>(replacement);
        } else {
            before.clear();
            instead.clear();
            after.clear();
        }

        /* No replacement for tag -> use original content */
        if (instead.empty()) {
            instead = content;
        }

        before_it = before.begin();
        instead_it = instead.begin();
        after_it = after.begin();
    }

    std::streambuf::int_type buffer_peek()
    {
        if (!before.empty() && before_it != before.end()) {
            return traits_type::to_int_type(*before_it);
        }

        if (!instead.empty() && instead_it != instead.end()) {
            return traits_type::to_int_type(*instead_it);
        }

        if (!after.empty() && after_it != after.end()) {
            return traits_type::to_int_type(*after_it);
        }

        return traits_type::eof();
    }

    bool buffer_read()
    {
        if (!before.empty() && before_it != before.end()) {
            before_it++;
            return true;
        }

        if (!instead.empty() && instead_it != instead.end()) {
            instead_it++;
            return true;
        }

        if (!after.empty() && after_it != after.end()) {
            after_it++;
            return true;
        }

        return false;
    }

/* START HTML tag parser */
    std::stack<std::streampos> positions;

#define SAVE do { positions.push(pos); } while (0)
#define DROP do { positions.pop(); } while (0)
#define ROLLBACK do { if (pos != positions.top()) sbuf_p->pubseekpos(pos = positions.top()); positions.pop(); } while (0)
#define FAILURE do { ROLLBACK; return false; } while (0)
#define SUCCESS do { DROP; return true; } while (0)

    std::streambuf::char_type peek()
    {
        std::streambuf::int_type c = sbuf_p->sgetc();
        if (c == traits_type::eof())
            throw c;
        return traits_type::to_char_type(c);
    }

    std::streambuf::char_type read()
    {
        std::streambuf::int_type c = sbuf_p->sbumpc();
        if (c == traits_type::eof())
            throw c;
        return traits_type::to_char_type(c);
    }

    bool try_read(
        std::streambuf::char_type c,
        std::string& content,
        std::streampos& pos)
    {
        if (peek() != c)
            return false;
        content += read();
        pos += std::streamoff(1);
        return true;
    }

    bool try_read(
        std::streambuf::char_type c1,
        std::streambuf::char_type c2,
        std::string& content,
        std::streampos& pos)
    {
        std::streambuf::char_type c = peek();
        if (c1 > c || c > c2)
            return false;
        content += read();
        pos += std::streamoff(1);
        return true;
    }

    bool try_read(
        const std::string& str,
        std::string& content,
        std::streampos& pos)
    {
        SAVE;

        for (auto c : str)
        {
            if (!try_read(c, content, pos)) FAILURE;
        }

        SUCCESS;
    }

    bool try_not_read(
        const std::streambuf::char_type *s, size_t n,
        std::string& content,
        std::streampos& pos)
    {
        std::streambuf::char_type c = peek();
        for (size_t i = 0; i < n; ++i)
        {
            if (s[i] == c) return false;
        }
        content += read();
        pos += std::streamoff(1);
        return true;
    }

    template <typename funcT>
    bool read_any(
        funcT func,
        std::string& content,
        std::streampos& pos)
    {
        while (func(content, pos));
        return true;
    }

    template <typename funcT1, typename funcT2>
    bool try_read_many(
        funcT1 init,
        funcT2 loop,
        std::string& content,
        std::streampos& pos)
    {
        return init(content, pos) && read_any(loop, content, pos);
    }

    template <typename funcT>
    bool try_read_many(
        funcT func,
        std::string& content,
        std::streampos& pos)
    {
        return try_read_many(func, func, content, pos);
    }

    bool try_read_space(
        std::string& content,
        std::streampos& pos)
    {
        return try_read('\x20', content, pos)
            || try_read('\x09', content, pos)
            || try_read('\x0D', content, pos)
            || try_read('\x0A', content, pos);
    }

    bool read_any_spaces(
        std::string& content,
        std::streampos& pos)
    {
        return read_any([this](auto& c, auto& p) {
            return try_read_space(c, p);
        }, content, pos);
    }

    bool try_read_spaces(
        std::string& content,
        std::streampos& pos)
    {
        return try_read_many([this](auto& c, auto& p) {
            return try_read_space(c, p);
        }, content, pos);
    }

    bool try_read_equal(
        std::string& content,
        std::streampos& pos)
    {
        SAVE;

        read_any_spaces(content, pos);
        if (!try_read('=', content, pos)) FAILURE;
        read_any_spaces(content, pos);

        SUCCESS;
    }

    bool try_read_digit(
        std::string& content,
        std::streampos& pos)
    {
        return try_read('0', '9', content, pos);
    }

    bool try_read_hex_digit(
        std::string& content,
        std::streampos& pos)
    {
        return try_read_digit(content, pos)
            || try_read('a', 'f', content, pos)
            || try_read('A', 'F', content, pos);
    }

    bool try_read_number(
        std::string& content,
        std::streampos& pos)
    {
        return try_read_many([this] (auto& c, auto& p) {
            return try_read_digit(c, p);
        }, content, pos);
    }

    bool try_read_hex_number(
        std::string& content,
        std::streampos& pos)
    {
        return try_read_many([this] (auto& c, auto& p) {
            return try_read_hex_digit(c, p);
        }, content, pos);
    }

    bool try_read_name_start_char(
        std::string& content,
        std::streampos& pos)
    {
        if (try_read(':', content, pos)) return true;
        if (try_read('A', 'Z', content, pos)) return true;
        if (try_read('_', content, pos)) return true;
        if (try_read('a', 'z', content, pos)) return true;
        /* TODO: read UTF-8 char */
        // if (try_read('\u00C0', '\u00D6', content, pos)) return true;
        // if (try_read('\u00D8', '\u00F6', content, pos)) return true;
        // if (try_read('\u00F8', '\u02FF', content, pos)) return true;
        // if (try_read('\u0370', '\u037D', content, pos)) return true;
        // if (try_read('\u037F', '\u1FFF', content, pos)) return true;
        // if (try_read('\u200C', '\u200D', content, pos)) return true;
        // if (try_read('\u2070', '\u218F', content, pos)) return true;
        // if (try_read('\u2C00', '\u2FEF', content, pos)) return true;
        // if (try_read('\u3001', '\uD7FF', content, pos)) return true;
        // if (try_read('\uF900', '\uFDCF', content, pos)) return true;
        // if (try_read('\uFDF0', '\uFFFD', content, pos)) return true;
        // if (try_read('\U00010000', '\U000EFFFF', content, pos)) return true;

        return false;
    }

    bool try_read_name_char(
        std::string& content,
        std::streampos& pos)
    {
        if (try_read_name_start_char(content, pos)) return true;
        if (try_read('-', content, pos)) return true;
        if (try_read('.', content, pos)) return true;
        if (try_read('0', '9', content, pos)) return true;
        /* TODO: read UTF-8 char */
        // if (try_read('\u00B7', content, pos)) return true;
        // if (try_read('\u0300', '\u036F', content, pos)) return true;
        // if (try_read('\u203F', '\u2040', content, pos)) return true;

        return false;
    }

    bool try_read_name(
        std::string& content,
        std::streampos& pos)
    {
        return try_read_many(
            [this](auto& c, auto& p) { return try_read_name_start_char(c, p); },
            [this](auto& c, auto& p) { return try_read_name_char(c, p); },
            content, pos
        );
    }

    bool try_read_reference(
        std::string& content,
        std::streampos& pos)
    {
        SAVE;

        if (!try_read('&', content, pos)) { DROP; return false; }

        if (try_read('#', content, pos)) {
            if (try_read('x', content, pos) || try_read('X', content, pos)) {
                if (try_read_hex_number(content, pos)) {
                    if (try_read(';', content, pos)) {
                        SUCCESS;
                    }
                    FAILURE;
                }
                FAILURE;
            } else if (try_read_number(content, pos)) {
                if (try_read(';', content, pos)) {
                    SUCCESS;
                }
                FAILURE;
            }
            FAILURE;
        } else if (try_read_name(content, pos)) {
            if (try_read(';', content, pos)) {
                SUCCESS;
            }
            FAILURE;
        }
        FAILURE;
    }

    bool try_read_unquoted_attribute_char(
        std::string& content,
        std::streampos& pos)
    {
        const std::streambuf::char_type excluded[] = {
            '\x20', '\x09', '\x0D', '\x0A', '"', '\'', '=', '<', '>', '`', '&',
        };
        return try_not_read(excluded, 11, content, pos);
    }

    bool try_read_attribute_char(
        std::streambuf::char_type q,
        std::string& content,
        std::streampos& pos)
    {
        const std::streambuf::char_type excluded[] = {'<', '&', q};
        return try_not_read(excluded, 3, content, pos);
    }

    template <typename funcT>
    bool try_read_inner_attribute_value(
        funcT func,
        std::string& content,
        std::streampos& pos)
    {
        bool ret = false;
        while (true) {
            if (func(content, pos)) {
                ret = true;
                continue;
            }

            if (try_read_reference(content, pos)) {
                ret = true;
                continue;
            }
            break;
        }

        return ret;
    }

    bool try_read_attribute_value(
        std::string& content,
        std::streampos& pos)
    {
        std::streambuf::char_type q;
        SAVE;

        if (try_read('"', content, pos) || try_read('\'', content, pos)) {
            q = content.back();

            // Don't check return as attribute value can be empty
            try_read_inner_attribute_value([this, q](auto& c, auto& p) {
                return try_read_attribute_char(q, c, p);
            }, content, pos);

            if (!try_read(q, content, pos)) FAILURE;

            SUCCESS;
        } else {
            bool ret = try_read_inner_attribute_value([this](auto& c, auto& p) {
                return try_read_unquoted_attribute_char(c, p);
            }, content, pos);
            if (!ret) FAILURE;

            SUCCESS;
        }
    }

    bool try_read_attribute(
        attribute& attribute,
        std::string& content,
        std::streampos& pos)
    {
        attribute.first.clear();
        attribute.second.clear();
        SAVE;

        if (!try_read_name(attribute.first, pos)) { DROP; return false; }
        content += attribute.first;
        // Attribute name without value
        if (!try_read_equal(content, pos)) SUCCESS;
        if (!try_read_attribute_value(attribute.second, pos)) FAILURE;
        content += attribute.second;

        SUCCESS;
    }

    bool try_read_tag(
        tag_type& type,
        tag_name& name,
        attributes& attributes,
        std::string& content,
        std::streampos& pos)
    {
        type = tag_type::start;
        name.clear();
        attributes.clear();
        content.clear();

        std::string spaces_content, attribute_content;
        attribute attribute;
        SAVE;

        if (!try_read('<', content, pos)) { DROP; return false; }
        if (try_read('/', content, pos)) {
            type = tag_type::end;
        }
        if (!try_read_name(name, pos)) FAILURE;
        content += name;
        if (type == tag_type::start) {
            while (true) {
                if (!try_read_spaces(content, pos)) { break; }
                if (!try_read_attribute(attribute, content, pos)) { break; }
                attributes.insert(attribute);
            }
        }
        read_any_spaces(content, pos);
        if (type == tag_type::start && try_read('/', content, pos)) {
            type = tag_type::empty;
        }
        if (!try_read('>', content, pos)) FAILURE;

        SUCCESS;
    }
/* END HTML tag parser */
}; // class streambuf

} // namespace html

#endif /* HTMLBUF_H_INCLUDED */
