#ifndef STRING_VIEWBUF_H_INCLUDED
#define STRING_VIEWBUF_H_INCLUDED

#include <array>
#include <list>
#include <streambuf>
#include <string_view>
#include <utility>

namespace std {

// Helper function to generate sequence
template <class T, T ... I >
static constexpr std::array<T, sizeof...(I) >  build_array(std::integer_sequence<T, I...>) noexcept
{
   return std::array<T, sizeof...(I) > { I... };
}

class string_viewbuf : public std::streambuf
{
public:
    string_viewbuf()
    :   views()
    ,   it(views.end())
    ,   pos(0)
    {}

    virtual std::streamsize xsputn (const char* s, std::streamsize n)
    {
        if (views.empty()) {
            views.emplace_back(s, n);
            it = views.begin();
            setg((char*)it->begin(), (char*)it->begin(), (char*)it->end());
        } else {
            views.emplace_back(s, n);
        }
        return views.back().size();
    }

    virtual std::streambuf::int_type overflow (std::streambuf::int_type c = EOF)
    {
        xsputn((const char*)&singles[c], 1);
        return traits_type::not_eof(c);
    }


    virtual std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way, std::ios_base::openmode which = std::ios_base::in | std::ios_base::out)
    {
        if (!(which & std::ios_base::in))
            return -1;

        if (views.empty())
            return 0;

        if (way == std::ios_base::cur && off == 0)
            return pos + (gptr() - eback());

        if (way == std::ios_base::beg) {
            std::size_t offset = off < 0 ? 0 : off;
            it = views.begin();
            pos = 0;
            while (offset > 0) {
                if (it->size() > offset)
                    break;
                if (std::next(it) == views.end()) {
                    offset = std::min(it->size(), offset);
                    break;
                }
                offset -= it->size();
                pos += it->size();
                it++;
            }

            setg((char*)it->begin(), (char*)it->begin() + offset, (char*)it->end());
            return pos + (gptr() - eback());
        }

        return -1;
    }

    virtual std::streampos seekpos(std::streampos sp, std::ios_base::openmode which = std::ios_base::in | std::ios_base::out)
    {
        return seekoff(sp, std::ios_base::beg, which);
    }


    virtual std::streambuf::int_type underflow()
    {
        if (views.empty())
            return traits_type::eof();

        if (gptr() == egptr()) {
            if (std::next(it) == views.end()) {
                return traits_type::eof();
            }
            pos += it->size();
            ++it;
        }

        setg((char*)it->begin(), (char*)it->begin(), (char*)it->end());

        return traits_type::to_int_type(*it->begin());
    }

    virtual std::streambuf::int_type pbackfail (std::streambuf::int_type c = EOF)
    {
        if (views.empty() || it == views.begin())
            return traits_type::eof();

        it--;
        setg((char*)it->begin(), (char*)it->end()-1, (char*)it->end());

        return traits_type::to_int_type(*gptr());
    }

private:
    template <class T>
    std::streamoff apply_offset(std::streamoff off, T& it, const T& end) {
        if (off < 0) off = 0;
        while (it != end && off > 0) {
            if (it->size() > (size_t)off) {
                break;
            } else {
                off -= it->size();
                it++;
            }
        }
        return off;
    }

private:
    std::list<std::string_view> views;
    std::list<std::string_view>::const_iterator it;
    std::streampos pos;

    static constexpr std::array singles = build_array(std::make_integer_sequence<std::streambuf::int_type, std::numeric_limits<std::streambuf::char_type>::max()>{});
}; // class string_viewbuf

} // namespace std

#endif /* STRING_VIEWBUF_H_INCLUDED */
