//---------------------------------------------------------
// Copyright 2015 Ontario Institute for Cancer Research
// Written by Matei David (matei@cs.toronto.edu)
//---------------------------------------------------------

// Reference:
// http://stackoverflow.com/questions/14086417/how-to-write-custom-input-stream-in-c

#ifndef __ZSTR_HPP
#define __ZSTR_HPP

#include <list>
#include <map>
#include <zlib.h>

namespace zlib {

class exception : public std::exception
{
public:
    exception(z_stream * zstrm_p, int ret)
        : msg("zlib: ")
    {
        switch (ret)
        {
        case Z_STREAM_ERROR:
            msg += "Z_STREAM_ERROR: ";
            break;
        case Z_DATA_ERROR:
            msg += "Z_DATA_ERROR: ";
            break;
        case Z_MEM_ERROR:
            msg += "Z_MEM_ERROR: ";
            break;
        case Z_VERSION_ERROR:
            msg += "Z_VERSION_ERROR: ";
            break;
        case Z_BUF_ERROR:
            msg += "Z_BUF_ERROR: ";
            break;
        default:
            msg += "[" + std::to_string(ret) + "]: ";
            break;
        }
        msg += zstrm_p->msg;
    }
    exception(const std::string msg) : msg(msg) {}
    const char * what() const noexcept { return msg.c_str(); }
private:
    std::string msg;
}; // class exception

class stream_wrapper : public z_stream
{
public:
    stream_wrapper()
    {
        this->zalloc = Z_NULL;
        this->zfree = Z_NULL;
        this->opaque = Z_NULL;
        this->avail_in = 0;
        this->next_in = Z_NULL;

        int ret = inflateInit2(this, 15+32);
        if (ret != Z_OK) throw exception(this, ret);
    }

    ~stream_wrapper()
    {
        inflateEnd(this);
    }
}; // class stream_wrapper

class streambuf
    : public std::streambuf
{
public:
    streambuf(
        std::streambuf *sbuf_p,
        std::size_t buff_size = (1 << 20),
        bool auto_detect = true
    ) : sbuf_p(sbuf_p)
    ,   in_buff(new char[buff_size]())
    ,   in_buff_start(in_buff)
    ,   in_buff_end(in_buff)
    ,   zstrm_p(new stream_wrapper())
    ,   buff_size(buff_size)
    ,   auto_detect(auto_detect)
    ,   auto_detect_run(false)
    ,   is_text(false)
    {
        if (!sbuf_p) throw exception("Missing z_stream wrapper");

        in_buff = new char [buff_size]();
        in_buff_start = in_buff_end = in_buff;
        setg(nullptr, nullptr, nullptr);
    }

    streambuf(const streambuf &) = delete;
    streambuf(streambuf &&) = default;
    streambuf & operator = (const streambuf &) = delete;
    streambuf & operator = (streambuf &&) = default;

    virtual ~streambuf()
    {
        delete [] in_buff;
        if (zstrm_p) delete zstrm_p;
    }

    virtual std::streamsize xsputn (const char* s, std::streamsize n)
    {
        return sbuf_p->sputn(s, n);
    }

    virtual std::streambuf::int_type overflow (std::streambuf::int_type c = EOF)
    {
        return sbuf_p->sputc(c);
    }


    virtual std::streampos seekoff(std::streamoff off, std::ios_base::seekdir way, std::ios_base::openmode which = std::ios_base::in | std::ios_base::out)
    {
        if (!(which & std::ios_base::in))
            return -1;

        if (out_buffs.empty())
            return 0;

        if (way == std::ios_base::cur && off == 0) {
            return out_pos + (gptr() - eback());
        }

        if (way == std::ios_base::beg) {
            std::size_t offset = off < 0 ? 0 : off;
            out_it = out_buffs.begin();
            out_pos = 0;
            while (offset > 0) {
                if (out_it->size() > offset) {
                    break;
                }
                if (std::next(out_it) == out_buffs.end() && !get_output()) {
                    offset = std::min(out_it->size(), offset);
                    break;
                }
                offset -= out_it->size();
                out_pos += out_it->size();
                out_it++;
            }

            setg(out_it->data(), out_it->data() + offset, out_it->data() + out_it->size());
            return out_pos + (gptr() - eback());
        }

        return -1;
    }

    virtual std::streampos seekpos(std::streampos sp, std::ios_base::openmode which = std::ios_base::in | std::ios_base::out)
    {
        return seekoff(sp, std::ios_base::beg, which);
    }


    virtual std::streambuf::int_type underflow()
    {
        bool first_read = out_buffs.empty();
        if (first_read || std::next(out_it) == out_buffs.end()) {
            if (!get_output())
                return traits_type::eof();
        }

        if (first_read) {
            out_pos = 0;
            out_it = out_buffs.begin();
        } else {
            out_pos += out_it->size();
            out_it++;
        }

        setg(out_it->data(), out_it->data(), out_it->data() + out_it->size());
        return traits_type::to_int_type(*this->gptr());
    }

private:
    bool is_last(std::list<std::string>::const_iterator& cit)
    {
        if (std::next(cit) != out_buffs.end()) return false;

        return get_output();
    }

    bool get_output() {
        if (!zstrm_p)
            return false;

        std::streamsize sz = sbuf_p->sgetn(in_buff, buff_size);
        if (!sz) return false;
        in_buff_start = in_buff;
        in_buff_end = in_buff + sz;

        if (auto_detect && ! auto_detect_run) {
            auto_detect_run = true;
            unsigned char b0 = *reinterpret_cast< unsigned char * >(in_buff);
            unsigned char b1 = *reinterpret_cast< unsigned char * >(in_buff + 1);
            // Ref:
            // http://en.wikipedia.org/wiki/Gzip
            // http://stackoverflow.com/questions/9050260/what-does-a-zlib-header-look-like
            is_text = !(
                in_buff + 2 <= in_buff_end && (
                    (b0 == 0x1F && b1 == 0x8B) ||   // gzip header
                    (b0 == 0x78 && (                // zlib header
                        b1 == 0x01 ||
                        b1 == 0x5E ||
                        b1 == 0x9C ||
                        b1 == 0xDA)
                    )
                )
            );
        }

        if (is_text) {
            out_buffs.emplace_back(in_buff_start, in_buff_end - in_buff_start);
            in_buff_start = in_buff_end = in_buff;
            return true;
        }

        do {
            // Create output buffer
            out_buffs.emplace_back(buff_size, 0);
            auto& out_buff = out_buffs.back();

            // Setup zstream
            zstrm_p->next_in = reinterpret_cast< decltype(zstrm_p->next_in) >(in_buff_start);
            zstrm_p->avail_in = in_buff_end - in_buff_start;
            zstrm_p->next_out = reinterpret_cast< decltype(zstrm_p->next_out) >(out_buff.data());
            zstrm_p->avail_out = buff_size;

            // run inflate() on input
            int ret = inflate(zstrm_p, Z_NO_FLUSH);
            if (ret != Z_OK && ret != Z_STREAM_END)
                throw exception(zstrm_p, ret);

            // Process inflate result
            in_buff_start = reinterpret_cast< decltype(in_buff_start) >(zstrm_p->next_in);
            in_buff_end = in_buff_start + zstrm_p->avail_in;
            out_buff.resize(out_buff.size() - zstrm_p->avail_out);

            if (ret == Z_STREAM_END) {
                delete zstrm_p;
                zstrm_p = nullptr;
            }
        } while (in_buff_start != in_buff_end);

        return true;
    }

private:
    std::streambuf *sbuf_p;

    char *in_buff;
    char *in_buff_start;
    char *in_buff_end;

    std::list<std::string> out_buffs;
    std::list<std::string>::iterator out_it;
    std::size_t out_pos;

    stream_wrapper *zstrm_p;
    std::size_t buff_size;
    bool auto_detect;
    bool auto_detect_run;
    bool is_text;

}; // class streambuf

} // namespace zlib

#endif
