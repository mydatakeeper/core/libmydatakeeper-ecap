#include "service.h"

#include <libecap/adapter/xaction.h>
#include <libecap/common/autoconf.h>
#include <libecap/common/registry.h>
#include <libecap/common/name.h>

#include "config.h"
#include "dbus.h"
#include "log.h"
#include "utils.h"
#include "xaction_reqmod.h"
#include "xaction_respmod.h"

namespace {
    static const std::string REQMOD = "reqmod";
    static const std::string RESPMOD = "respmod";
} /* namespace anonymous */

mdk::adapter::Service::Service(const std::string& mode)
:   mode(mode)
,   bus("system")
,   adaptor(nullptr)
,   leaseFile()
,   knownIPs()
{}

mdk::adapter::Service::~Service()
{
    if (adaptor) delete adaptor;
}

std::string mdk::adapter::Service::uri() const {
    return "ecap://mydatakeeper.fr/ecap/services/modifier?mode=" + mode;
}

std::string mdk::adapter::Service::tag() const {
    return PROJECT_VERSION;
}

void mdk::adapter::Service::describe(std::ostream &os) const {
    os << "Mydatakeeper modifying service v" PROJECT_VERSION;
}

void mdk::adapter::Service::configure(const libecap::Options &config) {
    config.visitEachOption(*this);

    if (mode == RESPMOD) {
        for (auto rule : ruleParameters) {
            if (rule.where == RulePosition::BeforeTag)
                rules[{rule.tagName, rule.what}].before = rule.content;
            else /* if (rule.where == RulePosition::AfterTag) */
                rules[{rule.tagName, rule.what}].after = rule.content;
        }
    }

    if (mode == REQMOD) {
        adaptor = new DbusAdaptor(bus);
        adaptor->runSignalThread();
    }
}

void mdk::adapter::Service::visit(const libecap::Name &name, const libecap::Area &area) {
    if (name.assignedHostId()) return;

    std::string value(area.start, area.size);
    Log() << "Parsing '" << name.image() << "' value";

    if (mode == REQMOD) {
        /* Captive portal */
        if (name.image() == "leasefile") {
            leaseFile = value;
            Log(libecap::ilDebug) << "Reading content from " << leaseFile;
            getFileLines(leaseFile.c_str(), knownIPs);
            Log(libecap::ilDebug) << "There are " << knownIPs.size() << " known leases";
            return;
        } else if (name.image() == "bus") {
            bus = value;
            return;
        }
    } else /* if (mode == RESPMOD) */ {
        RuleParameter r;
        if (parseRule(r, name.image())) {
            if (r.how == RuleContentType::File) {
                if (!isFile(value)) {
                    Log(libecap::ilCritical) << "Not a file : " << value;
                    return;
                }
                Log(libecap::ilDebug) << "Reading content from " << value;
                r.content = getFileContent(value.c_str());
            } else /* if (r.how == RuleContentType::String) */ {
                r.content = value;
            }
            Log(libecap::ilDebug) << "Rule added";
            ruleParameters.push_back(r);
            return;
        }
    }

    Log(libecap::ilCritical) << "Unknown parameter '" << name.image() << "'";
}

void mdk::adapter::Service::reconfigure(const libecap::Options &cfg) {}

void mdk::adapter::Service::start() {
    libecap::adapter::Service::start();
    // custom code would go here, but this service does not have one
}

void mdk::adapter::Service::stop() {
    // custom code would go here, but this service does not have one
    libecap::adapter::Service::stop();
}

void mdk::adapter::Service::retire() {
    // custom code would go here, but this service does not have one
    libecap::adapter::Service::stop();
}

bool mdk::adapter::Service::wantsUrl(const char *url) const {
    return true; // no-op is applied to all messages
}

mdk::adapter::Service::MadeXactionPointer
mdk::adapter::Service::makeXaction(libecap::host::Xaction *hostx) {
    if (mode == REQMOD) {
        return mdk::adapter::Service::MadeXactionPointer(
            new mdk::adapter::XactionReqmod(
                hostx,
                adaptor,
                leaseFile,
                knownIPs
            )
        );
    } else /* if (mode == RESPMOD) */ {
        return mdk::adapter::Service::MadeXactionPointer(
            new mdk::adapter::XactionRespmod(
                hostx,
                rules
            )
        );
    }
}

// create the adapter and register with libecap to reach the host application
static bool Register(const std::string &mode)
{
    return libecap::RegisterVersionedService(new mdk::adapter::Service(mode));
}

static const bool RegisteredReqmod = Register(REQMOD);
static const bool RegisteredRespmod = Register(RESPMOD);
