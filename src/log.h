#ifndef LOG_H_INCLUDED
#define LOG_H_INCLUDED

#include <libecap/common/autoconf.h>
#include <libecap/common/log.h>
#include <libecap/common/registry.h>
#include <libecap/host/host.h>

#include <iostream>

struct Log {
public:
    Log(libecap::LogVerbosity::Mask lv = libecap::flApplication|libecap::ilNormal)
        : os(libecap::MyHost().openDebug(lv)) {}
    ~Log() { if (os) libecap::MyHost().closeDebug(os); }

    template <class T>
    const Log &operator <<(const T &msg) const {
        if (os)
            *os << msg;
        return *this;
    }

private:
    std::ostream *os;
};

#endif /* LOG_H_INCLUDED */
