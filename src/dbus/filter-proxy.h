#ifndef FILTER_PROXY_H_INCLUDED
#define FILTER_PROXY_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "dbus/fr.mydatakeeper.filter.h"

extern const string FilterPath;
extern const string FilterName;

class FilterProxy
: public fr::mydatakeeper::filter_proxy,
  public DBusProxy
{
public:
    using ports_t = vector<uint16_t>;
    using relay_t = DBus::Struct<string, string, uint32_t, ports_t, ports_t>;
    using location_t = DBus::Struct<string, string, string, double, double, vector<relay_t>>;
    
    FilterProxy(DBus::Connection &connection)
    : DBusProxy(connection, FilterPath, FilterName) {}
    virtual ~FilterProxy() {}
};

#endif /* FILTER_PROXY_H_INCLUDED */
