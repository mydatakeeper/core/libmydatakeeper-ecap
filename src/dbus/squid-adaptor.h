#ifndef SQUID_ADAPTOR_H_INCLUDED
#define SQUID_ADAPTOR_H_INCLUDED

#include <mydatakeeper/dbus.h>

#include "dbus/org.squid_cache.proxy.h"

extern const string SquidPath;
extern const string SquidName;

class SquidAdaptor
: public DBusAdaptor,
  public org::squid_cache::proxy_adaptor
{
public:
    SquidAdaptor(DBus::Connection &connection)
    : DBusAdaptor(connection, SquidPath) {}
    virtual ~SquidAdaptor() {}
};

#endif /* FILTER_PROXY_H_INCLUDED */
