#ifndef XACTION_REQMOD_H_INCLUDED
#define XACTION_REQMOD_H_INCLUDED

#include <libecap/adapter/xaction.h>
#include <libecap/common/header.h>

#include <set>
#include <string>

class DbusAdaptor;

namespace mdk {
namespace adapter {

class XactionReqmod: public libecap::adapter::Xaction {
    public:
        XactionReqmod(
            libecap::host::Xaction *hostx,
            DbusAdaptor *adaptor,
            const std::string& leaseFile,
            std::set<std::string>& knownIps
        );
        virtual ~XactionReqmod();

        // meta-information for the host transaction
        virtual const libecap::Area option(const libecap::Name &name) const;
        virtual void visitEachOption(libecap::NamedValueVisitor &visitor) const;

        // lifecycle
        virtual void start();
        virtual void stop();

        // adapted body transmission control
        virtual void abDiscard();
        virtual void abMake();
        virtual void abMakeMore();
        virtual void abStopMaking();

        // adapted body content extraction and consumption
        virtual libecap::Area abContent(libecap::size_type offset, libecap::size_type size);
        virtual void abContentShift(libecap::size_type size);

        // virgin body state notification
        virtual void noteVbContentAvailable();
        virtual void noteVbContentDone(bool atEnd);

    private:
        libecap::host::Xaction *hostx; // Host transaction rep
        DbusAdaptor* adaptor;
        const std::string& leaseFile;
        std::set<std::string>& knownIps;
};

} /* adapter namespace */
} /* mdk namespace */

#endif /* XACTION_REQMOD_H_INCLUDED */