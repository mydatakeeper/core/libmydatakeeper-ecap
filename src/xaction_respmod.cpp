#include "xaction_respmod.h"

#include <string_view>
#include <unordered_set>

#include <libecap/common/errors.h>
#include <libecap/common/message.h>
#include <libecap/common/names.h>
#include <libecap/host/xaction.h>

#include "log.h"
#include "stream/htmlbuf.h"
#include "stream/string_viewbuf.h"
#include "stream/zstreambuf.h"

namespace {

const libecap::Name headerContentType("Content-Type", libecap::Name::NextId());
const libecap::Name headerContentEncoding("Content-Encoding", libecap::Name::NextId());

typedef std::unordered_set<std::string_view> set_type;
const set_type acceptedContentTypes {
    std::string_view("text/html"),

    // std::string_view("text/javascript"),
    // std::string_view("application/javascript"),
};

const set_type acceptedContentEncoding {
    std::string_view("gzip"),
    std::string_view("deflate"),
};

inline set_type::const_iterator findHeader(
    const libecap::Header& header,
    const libecap::Name& name,
    const set_type accepted)
{
    auto value = header.value(name);
    auto view = std::string_view(value.start, value.size);
    return std::find_if(accepted.cbegin(), accepted.cend(),
        [&view](const auto& acc) {
            return view.find(acc) == 0;
        }
    );
}

const std::string getUrl(const libecap::Message& message)
{
    auto requestLine = static_cast<const libecap::RequestLine*>(&message.firstLine());
    auto uri = requestLine->uri();
    return std::string(uri.start, uri.size);
}

} /* anonymous namespace */


mdk::adapter::XactionRespmod::XactionRespmod(
    libecap::host::Xaction *hostx,
    const Rules &rules
) : hostx(hostx)
,   rdbuf(nullptr)
,   zbuf(nullptr)
,   hbuf(nullptr)
,   stream(nullptr)

,   receivingVb(opUndecided)
,   sendingAb(opUndecided)

,   vbProductionFinished(false)
,   vbProductionAtEnd(false)
,   in_areas()

,   abProductionFinished(false)
,   abProductionAtEnd(false)
,   out_area()

,   rules(rules)
{}

mdk::adapter::XactionRespmod::~XactionRespmod()
{
    if (stream) delete stream;
    if (hbuf) delete hbuf;
    if (zbuf) delete zbuf;
    if (rdbuf) delete rdbuf;

    if (libecap::host::Xaction *x = hostx) {
        hostx = 0;
        x->adaptationAborted();
    }
}

const libecap::Area mdk::adapter::XactionRespmod::option(const libecap::Name &) const
{
    return libecap::Area();
}

void mdk::adapter::XactionRespmod::visitEachOption(libecap::NamedValueVisitor &) const
{}

void mdk::adapter::XactionRespmod::start()
{
    const libecap::Message& cause = hostx->cause();
    const std::string url = getUrl(cause);

    /* HTML body adaptor */
    Log(libecap::ilDebug)
        << "HTML adaptor: Adapting URL " << url << " response";

    if (rules.empty()) {
        Log(libecap::ilDebug)
            << "HTML adaptor: No rule available";
        hostx->useVirgin();
        receivingVb = opNever;
        return;
    }

    const libecap::Message& virgin = hostx->virgin();
    if (!virgin.body()) {
        Log(libecap::ilDebug)
            << "HTML adaptor: No body to adapt";
        hostx->useVirgin();
        receivingVb = opNever;
        return;
    }

    const libecap::Header& causeHeader = cause.header();
    if (causeHeader.hasAny(libecap::headerReferer)) {
        Log(libecap::ilDebug)
            << "HTML adaptor: Request is not the main page";
        hostx->useVirgin();
        receivingVb = opNever;
        return;
    }

    const libecap::Header& virginHeader = virgin.header();
    if (!setupContentTypeSupport(virginHeader)) {
        Log(libecap::ilDebug)
            << "HTML adaptor: Content type not supported";
        hostx->useVirgin();
        receivingVb = opNever;
        return;
    }

    if (!setupContentEncodingSupport(virginHeader)) {
        Log(libecap::ilDebug)
            << "HTML adaptor: Encoding type not supported";
        hostx->useVirgin();
        receivingVb = opNever;
        return;
    }

    // Create htmlbuf and stream
    setupStream();

    // ask host to supply virgin body
    hostx->vbMake();
    receivingVb = opOn;

    // Start adapting
    std::tr1::shared_ptr<libecap::Message> adapted = virgin.clone();
    libecap::Header& adaptedHeader = adapted->header();
    adaptedHeader.removeAny(libecap::headerContentLength);
    adaptedHeader.removeAny(headerContentEncoding);
    if (!adapted->body()) {
        sendingAb = opNever; // there is nothing to send
    }
    hostx->useAdapted(adapted);
}

bool mdk::adapter::XactionRespmod::setupContentTypeSupport(const libecap::Header& header)
{
    if (!header.hasAny(headerContentType))
        return false;

    auto it = findHeader(header, headerContentType, acceptedContentTypes);
    if (it == acceptedContentTypes.end())
        return false;

    // Create streambuf
    rdbuf = new std::string_viewbuf();
    return true;
}

bool mdk::adapter::XactionRespmod::setupContentEncodingSupport(const libecap::Header& header)
{
    if (!header.hasAny(headerContentEncoding))
        return true;

    auto it = findHeader(header, headerContentEncoding, acceptedContentEncoding);
    if (it == acceptedContentEncoding.end())
        return false;

    // Create zlib::streambuf
    zbuf = new zlib::streambuf(rdbuf);
    return true;
}

void mdk::adapter::XactionRespmod::setupStream()
{
    hbuf = new html::streambuf(zbuf ? (std::streambuf*)zbuf : (std::streambuf*)rdbuf);
    for (const auto& rule : rules)
    {
        auto& rule_content = rule.second;
        auto& tag = std::get<0>(rule.first);
        auto type = html::tag_type::none;
        switch(std::get<1>(rule.first)) {
            case StartTag:
                type = html::tag_type::start;
                break;
            case EndTag:
                type = html::tag_type::end;
                break;
            case EmptyTag:
                type = html::tag_type::empty;
                break;
            default:
                break;
        }
        hbuf->set_callback([&rule_content](auto type, const auto& tag, const auto& attrs) {
            return std::make_tuple(
                rule_content.before,
                std::string(),
                rule_content.after
            );
        }, tag, type);
    }

    stream = new std::iostream(hbuf);
    stream->exceptions(std::ios_base::badbit);
}


void mdk::adapter::XactionRespmod::stop()
{
    hostx = 0;
}

void mdk::adapter::XactionRespmod::abDiscard()
{
    Must(sendingAb == opUndecided); // have not started yet
    sendingAb = opNever;
    hostx->vbDiscard();
}

void mdk::adapter::XactionRespmod::abMake()
{
    Must(sendingAb == opUndecided); // have not yet started or decided not to send
    Must(hostx->virgin().body()); // that is our only source of ab content

    // we are or were receiving vb
    Must(receivingVb == opOn || receivingVb == opComplete);

    sendingAb = opOn;
    if (stream->good())
        hostx->noteAbContentAvailable();
}

void mdk::adapter::XactionRespmod::abMakeMore()
{
    Must(receivingVb == opOn); // a precondition for receiving more vb
    hostx->vbMakeMore();
}

void mdk::adapter::XactionRespmod::abStopMaking()
{
    sendingAb = opComplete;
    // we do not need more vb if the host is not interested in more ab
    stopVb();
}

libecap::Area mdk::adapter::XactionRespmod::abContent(libecap::size_type offset, libecap::size_type size)
{

    Must(sendingAb == opOn || sendingAb == opComplete);

    if (out_area.size > 0)
        return out_area;

    if (!stream->good())
        return libecap::Area();

    // Consume data from stream
    // Squid internal messaging buffer is of size (1 << 16)
    // but for some obscur reason, it will always leave a byte out.
    const size_t buff_size((1 << 16) - 1);
    char buff[buff_size];
    stream->read(buff, buff_size);

    return out_area = libecap::Area::FromTempBuffer(buff, stream->gcount());
}

void mdk::adapter::XactionRespmod::abContentShift(libecap::size_type offset)
{

    Must(sendingAb == opOn || sendingAb == opComplete);

    if (out_area.size == offset) {
        out_area = libecap::Area();
    } else {
        out_area = libecap::Area(out_area.start + offset, out_area.size - offset, out_area.details);
    }

    // TODO : free up in_areas memory when possible
}

void mdk::adapter::XactionRespmod::noteVbContentAvailable()
{
    Must(receivingVb == opOn);

    const libecap::Area vb = hostx->vbContent(0, libecap::nsize); // get all vb
    if (vb.size > 0) {
        stream->clear();
        stream->write(vb.start, vb.size);
        in_areas.emplace(vb.start, vb.size, vb.details);
        hostx->vbContentShift(vb.size);
        if (sendingAb == opOn)
            hostx->noteAbContentAvailable();
    }
}

void mdk::adapter::XactionRespmod::noteVbContentDone(bool atEnd)
{

    Must(receivingVb == opOn);

    stopVb();
    if (sendingAb == opOn) {
        hostx->noteAbContentDone(atEnd);
        sendingAb = opComplete;
    }
}

void mdk::adapter::XactionRespmod::stopVb() {
    if (receivingVb == opOn) {
        hostx->vbStopMaking(); // we will not call vbContent() any more
        receivingVb = opComplete;
    } else {
        // we already got the entire body or refused it earlier
        Must(receivingVb != opUndecided);
    }
}
