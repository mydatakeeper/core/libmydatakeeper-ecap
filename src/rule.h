#ifndef RULE_H_INCLUDED
#define RULE_H_INCLUDED

#include <string>
#include <list>
#include <map>

namespace mdk {
    using RuleTag = std::string;
    using RuleContent = std::string;

    enum RuleType { StartTag, EndTag, EmptyTag };
    enum RulePosition { BeforeTag, AfterTag };
    enum RuleContentType { File, String };

    struct RuleParameter {
        RuleTag tagName;
        RuleType what;
        RulePosition where;
        RuleContentType how;
        RuleContent content;
    };

    struct Rule {
        RuleContent before;
        RuleContent after;
    };

    using RuleKey = std::tuple<RuleTag, RuleType>; 
    using Rules = std::map<RuleKey, Rule>;
    using RuleParameters = std::list<RuleParameter>;

    bool parseRule(RuleParameter& rule, const std::string& str);
} // namespace mdk

#endif /* RULE_H_INCLUDED */
