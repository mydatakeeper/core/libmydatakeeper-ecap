#ifndef XACTION_RESPMOD_H_INCLUDED
#define XACTION_RESPMOD_H_INCLUDED

#include <libecap/adapter/xaction.h>
#include <libecap/common/header.h>

#include <queue>

#include "rule.h"

namespace html { class streambuf; }
namespace std  { class string_viewbuf; }
namespace zlib { class streambuf; }

namespace mdk {
namespace adapter {

class Service;

class XactionRespmod: public libecap::adapter::Xaction {
    public:
        XactionRespmod(
            libecap::host::Xaction *hostx,
            const Rules &rules
        );
        virtual ~XactionRespmod();

        // meta-information for the host transaction
        virtual const libecap::Area option(const libecap::Name &name) const;
        virtual void visitEachOption(libecap::NamedValueVisitor &visitor) const;

        // lifecycle
        virtual void start();
        virtual void stop();

        // adapted body transmission control
        virtual void abDiscard();
        virtual void abMake();
        virtual void abMakeMore();
        virtual void abStopMaking();

        // adapted body content extraction and consumption
        virtual libecap::Area abContent(libecap::size_type offset, libecap::size_type size);
        virtual void abContentShift(libecap::size_type size);

        // virgin body state notification
        virtual void noteVbContentAvailable();
        virtual void noteVbContentDone(bool atEnd);

    private:
        bool setupContentTypeSupport(const libecap::Header& header);
        bool setupContentEncodingSupport(const libecap::Header& header);
        void setupStream();

        void stopVb();

        libecap::host::Xaction *hostx; // Host transaction rep

        // Stream and buffers
        std::string_viewbuf *rdbuf;
        zlib::streambuf *zbuf;
        html::streambuf *hbuf;
        std::iostream *stream;

        typedef enum { opUndecided, opOn, opComplete, opNever } OperationState;
        OperationState receivingVb;
        OperationState sendingAb;

        // Input data
        bool vbProductionFinished;
        bool vbProductionAtEnd;
        std::queue<libecap::Area> in_areas;

        // Output data
        bool abProductionFinished;
        bool abProductionAtEnd;
        libecap::Area out_area;

        const Rules &rules;
};

} /* adapter namespace */
} /* mdk namespace */

#endif /* XACTION_RESPMOD_H_INCLUDED */