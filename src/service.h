#ifndef SERVICE_H_INCLUDED
#define SERVICE_H_INCLUDED

#include <libecap/adapter/service.h>
#include <libecap/common/named_values.h>

#include <ostream>
#include <string>
#include <set>

#include "rule.h"

class DbusAdaptor;

namespace mdk {
namespace adapter {

class Service: public libecap::adapter::Service, public libecap::NamedValueVisitor {
    public:
        Service(const std::string& mode);
        virtual ~Service();

        // About
        virtual std::string uri() const;
        virtual std::string tag() const;
        virtual void describe(std::ostream &os) const;

        // Configuration
        virtual void configure(const libecap::Options &cfg);
        virtual void reconfigure(const libecap::Options &cfg);
        virtual void visit(const libecap::Name &name, const libecap::Area &value);

        // void setOne(const std::string &name, const std::string &value);

        // Lifecycle
        virtual void start();
        virtual void stop();
        virtual void retire();

        // Scope (XXX: this may be changed to look at the whole header)
        virtual bool wantsUrl(const char *url) const;

        // Work
        virtual MadeXactionPointer makeXaction(libecap::host::Xaction *hostx);

    public:
        // Configuration storage
        const std::string& mode;

        // Request mode
        std::string bus;
        DbusAdaptor *adaptor;

        // Response mode
        Rules rules;
        RuleParameters ruleParameters;

        // Captive portal
        std::string leaseFile;
        std::set<std::string> knownIPs;
};

} /* adapter namespace */
} /* mdk namespace */

#endif /* SERVICE_H_INCLUDED */