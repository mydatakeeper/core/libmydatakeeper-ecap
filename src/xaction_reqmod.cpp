#include "xaction_reqmod.h"

#include <fstream>
#include <map>

#include <libecap/common/header.h>
#include <libecap/common/message.h>
#include <libecap/common/names.h>
#include <libecap/host/xaction.h>

#include <curl/curl.h>

#include "log.h"
#include "dbus.h"
#include "w3c/HTParse.h"

namespace {

const libecap::Name headerAcceptEncoding("Accept-Encoding", libecap::Name::NextId());
const std::string valueAcceptEncoding("gzip, deflate");
const libecap::Area areaAcceptEncoding(valueAcceptEncoding.c_str(), valueAcceptEncoding.size());

const std::string getSourceIp(libecap::host::Xaction *hostx)
{
    return hostx->option(libecap::metaClientIp).toString();
}

const std::string getUrl(const libecap::Message& message)
{
    auto requestLine = static_cast<const libecap::RequestLine*>(&message.firstLine());
    auto uri = requestLine->uri();
    return std::string(uri.start, uri.size);
}

const std::string getDomain(const std::string &url)
{
    HTURI uri(url);
    return std::string(uri.host);
}

const std::string getRefererDomain(const libecap::Message& message)
{
    const libecap::Header& header = message.header();
    if (!header.hasAny(libecap::headerReferer))
        return {};

    const auto referer = header.value(libecap::headerReferer);
    return getDomain(referer.start);
}

std::tr1::shared_ptr<libecap::Message> encoding(const libecap::Message& virgin)
{
    auto adapted = virgin.clone();
    adapted->header().removeAny(headerAcceptEncoding);
    // adapted->header().add(headerAcceptEncoding, areaAcceptEncoding);

    return adapted;
}

std::tr1::shared_ptr<libecap::Message> redirect(const std::string& url)
{
    auto redirect = libecap::MyHost().newResponse();
    auto status = static_cast<libecap::StatusLine*>(
        &redirect->firstLine()
    );
    status->statusCode(302);
    status->reasonPhrase(libecap::Area::FromTempString("Found"));
    auto& header = redirect->header();
    header.add({"Location"}, libecap::Area::FromTempString(url));
    return redirect;
}

std::string url_encode(const std::string &url)
{
    CURL *handle = curl_easy_init();
    if (!handle) {
        Log(libecap::ilCritical) << "Unable to start a libcurl easy session";
        return {};
    }

    char *encode = curl_easy_escape(handle, url.c_str(), url.size());
    if (!encode) {
        Log(libecap::ilCritical) << "Unable to escape URL";
        curl_easy_cleanup(handle);
        return {};
    }

    std::string encode_str(encode);
    curl_free(encode);

    curl_easy_cleanup(handle);
    return encode_str;
}

} /* namespace anonymous */

mdk::adapter::XactionReqmod::XactionReqmod(
    libecap::host::Xaction *hostx,
    DbusAdaptor *adaptor,
    const std::string& leaseFile,
    std::set<std::string>& knownIps
) : hostx(hostx)
,   adaptor(adaptor)
,   leaseFile(leaseFile)
,   knownIps(knownIps)
{}

mdk::adapter::XactionReqmod::~XactionReqmod()
{
    if (libecap::host::Xaction *x = hostx) {
        hostx = 0;
        x->adaptationAborted();
    }
}

const libecap::Area mdk::adapter::XactionReqmod::option(const libecap::Name &) const
{
    return libecap::Area();
}

void mdk::adapter::XactionReqmod::visitEachOption(libecap::NamedValueVisitor &) const {}

void mdk::adapter::XactionReqmod::start()
{
    const libecap::Message& virgin = hostx->virgin();

    const auto src = getSourceIp(hostx);
    const auto url = getUrl(virgin);
    const auto domain = getDomain(url);
    const auto refererDomain = getRefererDomain(virgin);

    /* Captive portal redirection */
    if (!leaseFile.empty() && knownIps.find(src) == knownIps.end()) {
        Log(libecap::ilDebug)
            << "Captive portal: IP address " << src << " is unknown";

        const std::set<std::string> whitelist = {
            "mydatakeeper.local",
            "local.mydatakeeper.fr",
            "home.mydatakeeper.fr",
        };
        const std::string secure_proto = "https://";
        const std::string redirect_proto = "http://";
        const std::string portal_url = "http://local.mydatakeeper.fr/certificate?url=";

        if (url.rfind(secure_proto, 0) == 0) {
            knownIps.insert(src);
            Log(libecap::ilDebug)
                << "Captive portal: IP address configured";

            std::ofstream outfile(leaseFile, std::ios_base::out | std::ios_base::app);
            if (outfile) {
                outfile << src << std::endl;
                outfile.close();
                Log(libecap::ilDebug)
                    << "Captive portal: Lease saved to " << leaseFile;
            } else {
                Log(libecap::ilCritical)
                    << "Captive portal: Lease NOT saved";
            }
        } else if (whitelist.find(domain) != whitelist.end()) {
            Log(libecap::ilDebug)
                << "Captive portal: Skipping whitelisted URL " << url;
        } else if (url.rfind(redirect_proto, 0) == 0) {
            Log(libecap::ilDebug)
                << "Captive portal: Redirecting " << url << " to portal";
            hostx->useAdapted(redirect(portal_url + url_encode(url)));
            return;
        } else {
            Log(libecap::ilDebug)
                << "Captive portal: Unsupported request type";
        }
    }

    auto adapted = encoding(virgin);

    /* Ad blocker */
    if (!adaptor) {
        Log(libecap::ilDebug) << "Ad blocker: No URL filter provided";
        hostx->useAdapted(adapted);
        return;
    }

    if (adaptor->filter(src, url, domain, refererDomain, "")) {
        Log(libecap::ilDebug) << "Ad blocker: URL blocked";
        hostx->blockVirgin();
    } else {
        Log(libecap::ilDebug) << "Ad blocker: URL NOT blocked";
        hostx->useAdapted(adapted);
    }
}

void mdk::adapter::XactionReqmod::stop()
{
    hostx = 0;
}

void mdk::adapter::XactionReqmod::abDiscard()
{
    hostx->vbDiscard();
}

void mdk::adapter::XactionReqmod::abMake()
{
    hostx->vbMake();
}

void mdk::adapter::XactionReqmod::abMakeMore()
{
    hostx->vbMakeMore();
}

void mdk::adapter::XactionReqmod::abStopMaking()
{
    hostx->vbStopMaking();
}

libecap::Area mdk::adapter::XactionReqmod::abContent(libecap::size_type off, libecap::size_type sz)
{
    return hostx->vbContent(off, sz);
}

void mdk::adapter::XactionReqmod::abContentShift(libecap::size_type sz)
{
    hostx->vbContentShift(sz);
}

void mdk::adapter::XactionReqmod::noteVbContentAvailable()
{
    hostx->noteAbContentAvailable();
}

void mdk::adapter::XactionReqmod::noteVbContentDone(bool done)
{
    hostx->noteAbContentDone(done);
}

