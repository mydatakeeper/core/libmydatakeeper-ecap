#include "rule.h"

#include "log.h"

bool mdk::parseRule(mdk::RuleParameter& rule, const std::string& str)
{
    std::list<int> indexes;
    for (auto it = str.rbegin(); it != str.rend(); ++it) {
        if (*it == '_') {
            indexes.push_front((str.rend() - it));
        }
    }
    if (indexes.size() != 3)
        return false;

    auto i0 = 0;
    auto i1 = indexes.front();
    indexes.pop_front();
    auto i2 = indexes.front();
    indexes.pop_front();
    auto i3 = indexes.front();
    indexes.pop_front();
    auto i4 = str.size();

    rule.tagName = std::string(str.c_str() + i0, i1 - i0 - 1);
    std::string_view what(str.c_str() + i1, i2 - i1 - 1);
    if (what == "starttag")
        rule.what = RuleType::StartTag;
    else if (what == "endtag")
        rule.what = RuleType::EndTag;
    else if (what == "emptytag")
        rule.what = RuleType::EmptyTag;
    else
        return false;

    std::string_view where(str.c_str() + i2, i3 - i2 - 1);
    if (where == "before")
        rule.where = RulePosition::BeforeTag;
    else if (where == "after")
        rule.where = RulePosition::AfterTag;
    else
        return false;

    std::string_view how(str.c_str() + i3, i4 - i3);
    if (how == "file")
        rule.how = RuleContentType::File;
    else if (how == "str")
        rule.how = RuleContentType::String;
    else
        return false;

    Log(libecap::ilDebug) << "Rule name (" << what << "," << where << "," << how << ") parsed";
    return true;
}
