#ifndef DBUS_H_INCLUDED
#define DBUS_H_INCLUDED

#include <string>
#include <queue>
#include <tuple>
#include <map>

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>

class FilterProxy;
class SquidAdaptor;

class DbusAdaptor {
public:
    DbusAdaptor(const std::string& bus);
    ~DbusAdaptor();

    bool filter(
        const std::string& src,
        const std::string& url,
        const std::string& domain,
        const std::string& refererDomain,
        const std::string& type);

    void runSignalThread();
    void stopSignalThread();

private:
    void signalRunner();
    
private:
    FilterProxy *proxy;
    SquidAdaptor *adaptor;

    std::atomic<bool> thread_running;
    std::thread thread_signal;
    std::mutex mtx_signal;
    std::condition_variable cv_signal;
    std::queue<std::tuple<std::string, std::string, std::string, std::map<std::string, std::string>>> signals;
};

#endif /* DBUS_H_INCLUDED */
