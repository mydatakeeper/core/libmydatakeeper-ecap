#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod";
    auto content = fileContent(CWD "/resources/large.html");
    auto request = Message::request("https://stackoverflow.com/");
    auto response = Message::response(
        {
            {"Content-Type", "text/html"},
        },
        content,
        {
            // Reading message chunk by chunk based on real run
            809, 5792, 7240, 1448, 1448, 1448, 2896, 4344, 1448, 1448, 1448,
            1448, 2896, 7240, 1448, 1448, 1448, 1448, 5792, 2896, 2896, 2896,
            2896, 2896, 2896, 1448, 1448, 4344, 1448, 2896, 4344, 1448, 1448,
            1448, 4344, 2896, 1448, 1448, 1448, 2896, 7240, 2896, 1448, 1448,
            1448, 1448, 1448, 1448, 4344, 4344, 2896, 1448, 1448, 1448, 2896,
            1448, 1448, 1448, 1448, 2896, 1448, 1448, 1448, 1448, 1448, 5792,
            1448, 1448, 1448, 2896, 2896, 2896, 2896, 4344, 1448, 1448, 2896,
            1448, 1448, 2896, 4344, 1448, 1448, 1448, 5792, 5792, 1448, 1448,
            429, 
        }
    );
    auto options = Options(
        {
            {"script_starttag_before_file", CWD "/resources/modifier/script_prepend.html"},
            {"script_endtag_after_file", CWD "/resources/modifier/script_append.html"},
            {"body_starttag_after_file", CWD "/resources/modifier/body_prepend.html"},
        }
    );
    const auto results = runRespmodTest(uri, request, response, options);

    auto expected = fileContent(CWD "/resources/large.html.adapted");
    std::string actual;
    for (const auto& lst : results)
        for (const auto& str : lst)
            actual += str;

    return expected == actual ? EXIT_SUCCESS : EXIT_FAILURE;
}
