#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod";
    auto content = fileContent(CWD "/resources/e-cap.org.html");
    auto request = Message::request("http://e-cap.org/");
    auto response = Message::response(
        {
            {"Content-Type", "text/html"},
        },
        content
    );
    auto options = Options(
        {
            {"script_starttag_before_file", CWD "/resources/modifier/script_prepend.html"},
            {"script_endtag_after_file", CWD "/resources/modifier/script_append.html"},
            {"body_starttag_after_file", CWD "/resources/modifier/body_prepend.html"},
        }
    );
    const auto results = runRespmodTest(uri, request, response, options, 128);

    auto expected = fileContent(CWD "/resources/e-cap.org.html.adapted");
    std::string actual;
    for (const auto& lst : results)
        for (const auto& str : lst)
            actual += str;

    return expected == actual ? EXIT_SUCCESS : EXIT_FAILURE;
}
