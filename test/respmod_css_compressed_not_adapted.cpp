#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod";
    auto content = fileContent(CWD "/resources/main.css");
    auto request = Message::request(
        "http://e-cap.org/assets/main.css",
        {
            {"Referer", "http://www.e-cap.org/"},
            {"Accept-Encoding", "gzip, deflate"},
        }
    );
    auto response = Message::response(
        {
            {"Content-Type", "text/css"},
            {"Content-Encoding", "gzip"},
        },
        content
    );
    auto options = Options(
        {
            {"script_starttag_before_file", CWD "/resources/modifier/script_prepend.html"},
            {"script_endtag_after_file", CWD "/resources/modifier/script_append.html"},
            {"body_starttag_after_file", CWD "/resources/modifier/body_prepend.html"},
        }
    );
    const auto results = runRespmodTest(uri, request, response, options);
    return results.empty() ? EXIT_SUCCESS : EXIT_FAILURE;
}
