#ifndef BODY_H_INCLUDED
#define BODY_H_INCLUDED

#include <libecap/common/body.h>

#include <list>

namespace test {

struct Body : public libecap::Body
{
    std::string _body;
    std::list<size_t> _chunks;
    libecap::size_type _offset;

    Body() : _body() {}
    Body(
        const std::string& str,
        std::list<size_t> chunks = {}
    ) : _body(str)
    ,   _chunks(chunks)
    ,   _offset(0)
    {
        if (_chunks.empty())
            _chunks.push_back(_body.size());
    }
    virtual ~Body() {}

    virtual libecap::BodySize bodySize() const
    {
        return libecap::BodySize(_body.size());
    }

    libecap::Area content(libecap::size_type offset, libecap::size_type size) const
    {
        if (_chunks.empty()) throw -1;
        if (offset != 0) throw -1;

        size = std::min(size, _body.size() - (_offset + offset));
        size = std::min(size, _chunks.front());

        return libecap::Area(_body.c_str() + _offset + offset, size);
    }

    void shift(libecap::size_type size)
    {
        if (_chunks.empty()) throw -1;
        if (size > _chunks.front()) throw -1;

        _offset += size;
        _chunks.front() -= size;
        if (_chunks.front() == 0)
            _chunks.pop_front();
    }

    bool done() const
    {
        return _offset == _body.size();
    }

};

} /* namespace test */

#endif /* BODY_H_INCLUDED */
