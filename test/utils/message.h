#ifndef MESSAGE_H_INCLUDED
#define MESSAGE_H_INCLUDED

#include <libecap/common/message.h>
#include "header.h"
#include "body.h"

namespace test {

struct Message : public libecap::Message
{
    using FirstLine_t = libecap::shared_ptr<FirstLine>;
    using Header_t = libecap::shared_ptr<Header>;
    using Body_t = libecap::shared_ptr<Body>;
    using Message_t = libecap::shared_ptr<Message>;

    FirstLine_t _firstLine;
    Header_t _header;
    Body_t _body;
    Header_t _trailer;

    Message(
        FirstLine_t firstLine = FirstLine_t(),
        Header_t header = Header_t(),
        Body_t body = Body_t(),
        Header_t trailer = Header_t()
    ) : _firstLine(firstLine)
    ,   _header(header)
    ,   _body(body)
    ,   _trailer(trailer) {}
    virtual ~Message() {}

    static Message_t request(
        const std::string& uri,
        Header::map_type headers = {},
        std::string body = std::string())
    {
        return Message_t(
            new Message(
                FirstLine_t(new RequestLine(libecap::Area::FromTempString(uri))),
                Header_t(new Header(headers)),
                body.empty() ? Body_t() : Body_t(new Body(body))
            )
        );
    }

    static Message_t response(
        Header::map_type headers = {},
        std::string body = std::string(),
        std::list<size_t> chunks = {})
    {
        return Message_t(
            new Message(
                FirstLine_t(new StatusLine()),
                Header_t(new Header(headers)),
                body.empty() ? Body_t() : Body_t(new Body(body, chunks))
            )
        );
    }

    virtual libecap::shared_ptr<libecap::Message> clone() const {
        return libecap::shared_ptr<libecap::Message>(
            new Message(
                _firstLine,
                _header,
                _body,
                _trailer
            )
        );
    }

    // always present, determines direction
    virtual libecap::FirstLine &firstLine() { return *_firstLine; } 
    virtual const libecap::FirstLine &firstLine() const { return *_firstLine; }

    virtual libecap::Header &header() { return *_header; }
    virtual const libecap::Header &header() const { return *_header; }

    virtual void addBody() { _body = Body_t(); }
    virtual libecap::Body *body() { return _body.get(); }
    virtual const libecap::Body *body() const { return _body.get(); }

    virtual void addTrailer() { _trailer = Header_t(); }
    virtual libecap::Header *trailer() { return _trailer.get(); }
    virtual const libecap::Header *trailer() const { return _trailer.get(); }
};

} /* namespace test */

#endif /* MESSAGE_H_INCLUDED */
