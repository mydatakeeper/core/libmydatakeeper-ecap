#ifndef XACTION_H_INCLUDED
#define XACTION_H_INCLUDED

#include <list>

#include <libecap/host/xaction.h>
#include <libecap/adapter/xaction.h>

#include "message.h"
#include "body.h"

namespace test {

struct Xaction : public libecap::host::Xaction
{
    using Service_t = libecap::shared_ptr<libecap::adapter::Service>;
    using Xaction_t = libecap::shared_ptr<libecap::adapter::Xaction>;
    using Message_t = libecap::shared_ptr<Message>;
    using AdaptedMessage_t = libecap::shared_ptr<libecap::Message>;

    Xaction(
        const Message_t request,
        const Message_t response = {},
        libecap::size_type vbBuffSize = (1 << 16) - 1,
        libecap::size_type abBuffSize = (1 << 16) - 1
    ) :
        _adapterXaction(),

        _request(request),
        _response(response),
        _adapted(),

        blocked(false),

        makingVb(true),
        vbProductionFinished(false),
        vbBuffSize(vbBuffSize),
        vbOffset(0),

        abBuffSize(abBuffSize),
        abBuff(abBuffSize),
        abContent(),
        abProductionFinished(false),
        abProductionAtEnd(false)
    {}
    virtual ~Xaction() {}

    virtual const libecap::Area option(const libecap::Name &name) const
    {
        return {};
    }
    virtual void visitEachOption(libecap::NamedValueVisitor &visitor) const {}

    // Adapter Xaction
    Xaction_t _adapterXaction;
    libecap::adapter::Xaction* makeXaction(Service_t& service)
    {
        _adapterXaction = service->makeXaction(this);
        return _adapterXaction.get();
    }

    // Messages
    const Message_t _request;
    const Message_t _response;
    AdaptedMessage_t _adapted;
    virtual libecap::Message &virgin()
    {
        if (_response.get())
            return *_response;
        return *_request;
    }
    virtual const libecap::Message &cause()
    {
        if (_response.get())
            return *_request;
        throw -1;
    }
    virtual libecap::Message &adapted() {
        if (hasAdapted())
            return *_adapted;
        throw -1;
    }

    // adaptation decision making
    bool blocked;
    virtual void useVirgin()
    {
        _adapted.reset();
    }
    virtual void useAdapted(const AdaptedMessage_t &msg)
    {
        _adapted = msg;
        if (msg->body()) {
            _adapterXaction->abMake();
        }
    }
    virtual void blockVirgin()
    {
        blocked = true;
    }
    inline bool hasAdapted() {
        return _adapted.get();
    }
    inline Message &usedMessage() {
        if (hasAdapted())
            return *static_cast<Message*>(_adapted.get());
        return *_response;
    }

    // adapter transaction state notifications
    virtual void adaptationDelayed(const libecap::Delay &) {}
    virtual void adaptationAborted() {}
    virtual void resume() {}

    // virgin body transmission control
    bool makingVb;
    bool vbProductionFinished;
    virtual void vbDiscard() { makingVb = false; }
    virtual void vbMake() { makingVb = true; }
    virtual void vbStopMaking() { makingVb = false; }
    virtual void vbMakeMore() { makingVb = true; }
    virtual void vbPause() {}
    virtual void vbResume() {}

    // virgin body content extraction and consumption
    const libecap::size_type vbBuffSize;
    libecap::size_type vbOffset;
    virtual libecap::Area vbContent(libecap::size_type off, libecap::size_type size)
    {
        return static_cast<Body*>(
            usedMessage().body()
        )->content(off, std::min(size, vbBuffSize));
    }
    virtual void vbContentShift(libecap::size_type size)
    {
        static_cast<Body*>(
            usedMessage().body()
        )->shift(size);
    }
    inline bool vbContentDone()
    {
        return static_cast<Body*>(
            usedMessage().body()
        )->done();
    }

    // adapted body state notification
    const libecap::size_type abBuffSize;
    libecap::size_type abBuff;
    std::list<std::list<std::string>> abContent;
    bool abContentAvailable;
    bool abProductionFinished;
    bool abProductionAtEnd;
    virtual void noteAbContentDone(bool atEnd)
    {
        abProductionFinished = true;
        abProductionAtEnd = atEnd;
        moveAbContent();
    }
    virtual void noteAbContentAvailable()
    {
        moveAbContent();
    }


    inline bool abBufferFull()
    {
        return abBuff == 0;
    }
    inline void abBufferFlush()
    {
        abBuff = abBuffSize;
    }
    void moveAbContent()
    {
        const auto area = _adapterXaction->abContent(0, libecap::nsize);
        abContentAvailable = area.size != 0;
        if (area.size == 0 && abProductionFinished) {
            makingVb = false;
        } else if (area.size > 0) {
            const auto used = std::min(area.size, abBuff);
            abBuff -= used;

            if (abContent.empty()) abContent.push_back({});
            abContent.back().emplace_back(area.start, used);
            if (used)
                _adapterXaction->abContentShift(used);
        }
    }
};

} /* namespace test */

#endif /* XACTION_H_INCLUDED */
