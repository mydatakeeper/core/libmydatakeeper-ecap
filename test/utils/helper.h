#ifndef HELPER_H_INCLUDED
#define HELPER_H_INCLUDED

#include "options.h"
#include "host.h"
#include "xaction.h"
#include "message.h"
#include "header.h"

#include <libecap/common/autoconf.h>
#include <libecap/common/registry.h>
#include <libecap/common/names.h>

#include <string>
#include <fstream>
#include <cassert>
#include <dlfcn.h>

#define AREA(str) libecap::Area(str, std::size(str)-1)
#define NAME(str) libecap::Name(str)

void* handle;

std::string fileContent(const char* filename)
{
    std::ifstream ifs(filename);
    return std::string(
        std::istreambuf_iterator<char>(ifs),
        std::istreambuf_iterator<char>()
    );    
}

bool runReqmodTest(
    const std::string uri,
    libecap::shared_ptr<test::Message> request,
    const test::Options options = test::Options())
{
    handle = dlopen(LIBECAP, RTLD_LAZY);

    auto host = new test::Host();
    auto ptr = libecap::shared_ptr<libecap::host::Host>(host);
    libecap::RegisterHost(ptr);

    auto service = host->adapterServices[uri];
    service->configure(options);
    service->start();

    auto requestLine = static_cast<const libecap::RequestLine*>(&request->firstLine());
    if (!service->wantsUrl(requestLine->uri().start))
        return {};

    auto hostXaction = new test::Xaction(request, {});
    auto adapterXaction = hostXaction->makeXaction(service);
    adapterXaction->start();

    dlclose(handle);

    return hostXaction->blocked;
}

std::list<std::list<std::string>> runRespmodTest(
    const std::string uri,
    libecap::shared_ptr<test::Message> request,
    libecap::shared_ptr<test::Message> response,
    const test::Options options = test::Options(),
    libecap::size_type in_buff_size = 1 << 16,
    libecap::size_type out_buff_size = 1 << 16)
{
    handle = dlopen(LIBECAP, RTLD_LAZY);

    auto host = new test::Host();
    auto ptr = libecap::shared_ptr<libecap::host::Host>(host);
    libecap::RegisterHost(ptr);

    auto service = host->adapterServices[uri];
    service->configure(options);
    service->start();

    auto requestLine = static_cast<const libecap::RequestLine*>(&request->firstLine());
    if (!service->wantsUrl(requestLine->uri().start))
        return {};

    auto hostXaction = new test::Xaction(request, response, in_buff_size, out_buff_size);
    auto adapterXaction = hostXaction->makeXaction(service);
    adapterXaction->start();

    assert(!hostXaction->blocked);

    if (hostXaction->hasAdapted()) {
        while (!hostXaction->vbContentDone()) {
            adapterXaction->noteVbContentAvailable();
            while (hostXaction->abContentAvailable) {
                if (hostXaction->abBufferFull()) {
                    hostXaction->abBufferFlush();
                }
                hostXaction->moveAbContent();
            }
        }
        adapterXaction->noteVbContentDone(true);
    }

    adapterXaction->stop();
    auto ret = hostXaction->abContent;
    delete hostXaction;
    service->stop();
    host->cleanupServices();
    dlclose(handle);

    return ret;
}

#endif /* HELPER_H_INCLUDED */
