#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

#include <map>

#include <libecap/common/header.h>
#include <libecap/common/name.h>
#include <libecap/common/names.h>
#include <libecap/common/version.h>

namespace test {

struct Header : public libecap::Header
{
    using map_type = std::map<std::string, std::string>;
    map_type headers;

    Header() {}
    Header(const map_type t) : headers(t) {}
    virtual ~Header() {}

    virtual bool hasAny(const libecap::Name &name) const
    {
        return headers.find(name.image()) != headers.end();
    }

    virtual libecap::Header::Value value(const libecap::Name &name) const
    {
        auto str = headers.at(name.image());
        return libecap::Area::FromTempString(str);
    }

    virtual void add(const libecap::Name &name, const libecap::Header::Value &value)
    {
        headers[name.image()] = std::string(value.start, value.size);
    }

    virtual void removeAny(const libecap::Name &name)
    {
        auto it = headers.find(name.image());
        if (it != headers.end())
            headers.erase(it);
    }

    virtual void visitEach(libecap::NamedValueVisitor &visitor) const
    {

    }

    virtual libecap::Area image() const
    {
        throw (char)'a';
    }
    virtual void parse(const libecap::Area &buf)
    {
        throw (void*)nullptr;
    }
};

struct FirstLine : public libecap::FirstLine
{
    libecap::Version _version;
    libecap::Name _protocol;

    FirstLine(
        libecap::Name protocol = libecap::protocolHttp,
        libecap::Version version = libecap::Version(1, 0, 0)
    ) :
        _version(version),
        _protocol(protocol)
    {}
    virtual ~FirstLine() {}

    virtual libecap::Version version() const { return _version; }
    virtual void version(const libecap::Version &aVersion) { _version = aVersion; }
    virtual libecap::Name protocol() const { return _protocol; }
    virtual void protocol(const libecap::Name &aProtocol) { _protocol = aProtocol; }
};

struct RequestLine: public test::FirstLine, public libecap::RequestLine {
    libecap::Area _uri;
    libecap::Name _method;

    RequestLine(
        libecap::Area uri,
        libecap::Name method = libecap::methodGet,
        libecap::Name protocol = libecap::protocolHttp,
        libecap::Version version = libecap::Version(1, 0, 0)
    ) :
        test::FirstLine(protocol, version),
        _uri(uri),
        _method(method)
    {}
    virtual ~RequestLine() {}

    virtual void uri(const libecap::Area &aUri) { _uri = aUri; }
    virtual libecap::Area uri() const { return _uri; }

    virtual void method(const libecap::Name &aMethod) { _method = aMethod; }
    virtual libecap::Name method() const { return _method; }

    virtual libecap::Version version() const { return test::FirstLine::version(); }
    virtual void version(const libecap::Version &aVersion) { test::FirstLine::version(aVersion); }
    virtual libecap::Name protocol() const { return test::FirstLine::protocol(); }
    virtual void protocol(const libecap::Name &aProtocol) { test::FirstLine::protocol(aProtocol); }
};

struct StatusLine: public test::FirstLine, public libecap::StatusLine {
    int _status;
    libecap::Area _reasonPhrase;

    StatusLine(
        int status = 200,
        libecap::Area reasonPhrase = libecap::Area(u8"OK", 2),
        libecap::Name protocol = libecap::protocolHttp,
        libecap::Version version = libecap::Version(1, 0, 0)
    ) :
        test::FirstLine(protocol, version),
        _status(status),
        _reasonPhrase(reasonPhrase)
    {}
    virtual ~StatusLine() {}

    virtual void statusCode(int code) { _status = code; }
    virtual int statusCode() const { return _status; }

    virtual void reasonPhrase(const libecap::Area &phrase) { _reasonPhrase = phrase; }
    virtual libecap::Area reasonPhrase() const { return _reasonPhrase; }

    virtual libecap::Version version() const { return test::FirstLine::version(); }
    virtual void version(const libecap::Version &aVersion) { test::FirstLine::version(aVersion); }
    virtual libecap::Name protocol() const { return test::FirstLine::protocol(); }
    virtual void protocol(const libecap::Name &aProtocol) { test::FirstLine::protocol(aProtocol); }
};

} /* namespace test */

#endif /* HEADER_H_INCLUDED */
