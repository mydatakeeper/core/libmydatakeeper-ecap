#ifndef HOST_H_INCLUDED
#define HOST_H_INCLUDED

#include <map>
#include <string>

#include <libecap/host/host.h>
#include <libecap/adapter/service.h>

namespace test {

struct Host : public libecap::host::Host
{
    using WeakService_t = libecap::weak_ptr<libecap::adapter::Service>;
    using Service_t = libecap::shared_ptr<libecap::adapter::Service>;
    using Services_t = std::map<std::string, Service_t>;
    using Message_t = libecap::shared_ptr<libecap::Message>;

    Host() {}
    virtual ~Host() {}

    virtual std::string uri() const
    {
        return "ecap://mydatakeeper.fr/ecap/host/test1";
    }
    virtual void describe(std::ostream &os) const
    {
        os << "Mydatakeeper host unit test 1";
    }

    virtual void noteVersionedService(
        const char *libEcapVersion,
        const WeakService_t &s)
    {
        auto ptr = s.lock();
        adapterServices[ptr->uri()] = ptr;
    }

    void cleanupServices()
    {
        adapterServices.clear();
    }

    virtual std::ostream *openDebug(libecap::LogVerbosity lv)
    {
        return &std::clog;
    }

    virtual void closeDebug(std::ostream *debug)
    {
        *debug << std::endl;
    }

    virtual Message_t newRequest() const
    {
        return {};
    }

    virtual Message_t newResponse() const
    {
        return {};
    }

    Services_t adapterServices;
};

} /* namespace test */

#endif /* HOST_H_INCLUDED */
