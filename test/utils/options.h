#ifndef OPTIONS_H_INCLUDED
#define OPTIONS_H_INCLUDED

#include <libecap/common/options.h>
#include <libecap/common/area.h>
#include <libecap/common/name.h>
#include <libecap/common/named_values.h>

#include <map>
#include <string>

namespace test {

struct Options : libecap::Options
{
    using values_type = std::map<std::string, std::string>;
    values_type values;

    Options(values_type v = {}) : values(v) {}
    virtual ~Options() {}

    virtual const libecap::Area option(const libecap::Name &name) const {
        const auto& value = values.at(name.image());
        return libecap::Area(value.c_str(), value.size());
    }

    virtual void visitEachOption(libecap::NamedValueVisitor &visitor) const {
        for (const auto& p : values) {
            libecap::Name name(p.first);
            visitor.visit(name, option(name));
        }
    }
};

} /* namespace test */

#endif /* OPTIONS_H_INCLUDED */
