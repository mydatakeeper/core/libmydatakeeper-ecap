#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=reqmod";
    auto request = Message::request(
        "http://balanderramed.site/"
    );
    bool blocked = runReqmodTest(uri, request);
    return blocked ? EXIT_SUCCESS : EXIT_FAILURE;
}
