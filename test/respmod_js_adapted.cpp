#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod";
    auto content = fileContent(CWD "/resources/util.js");
    auto request = Message::request(
        "http://e-cap.org/assets/util.js",
        {
            {"Referer", "http://www.e-cap.org/"},
        }
    );
    auto response = Message::response(
        {
            {"Content-Type", "application/javascript"},
        },
        content
    );
    auto options = Options(
        {
            {"script_starttag_before_file", CWD "/resources/modifier/script_prepend.html"},
            {"script_endtag_after_file", CWD "/resources/modifier/script_append.html"},
            {"body_starttag_after_file", CWD "/resources/modifier/body_prepend.html"},
        }
    );
    const auto results = runRespmodTest(uri, request, response, options);
    return results.empty() ? EXIT_SUCCESS : EXIT_FAILURE;

    // TODO : adapt JS body
    // auto expected = fileContent(CWD "/resources/util.js.adapted");
    // std::string actual;
    // for (const auto& lst : results)
    //     for (const auto& str : lst)
    //         actual += str;

    // return expected == actual ? EXIT_SUCCESS : EXIT_FAILURE;
}
