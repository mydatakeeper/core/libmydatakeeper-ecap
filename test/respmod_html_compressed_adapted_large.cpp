#include <iostream>

#include "utils/helper.h"

using namespace test;

int main ()
{
    auto uri = "ecap://mydatakeeper.fr/ecap/services/modifier?mode=respmod";
    auto content = fileContent(CWD "/resources/large.html.gz");
    auto request = Message::request(
        "https://stackoverflow.com/",
        {
            {"Accept-Encoding", "gzip, deflate"},
        }
    );
    auto response = Message::response(
        {
            {"Content-Type", "text/html"},
            {"Content-Encoding", "gzip"},
        },
        content
    );
    auto options = Options(
        {
            {"script_starttag_before_file", CWD "/resources/modifier/script_prepend.html"},
            {"script_endtag_after_file", CWD "/resources/modifier/script_append.html"},
            {"body_starttag_after_file", CWD "/resources/modifier/body_prepend.html"},
        }
    );
    const auto results = runRespmodTest(uri, request, response, options);

    auto expected = fileContent(CWD "/resources/large.html.adapted");
    size_t count = 0, cnt = 0;
    std::string actual;
    for (const auto& lst : results) {
        std::cerr << "Buffer" << count++ << " containing " << lst.size() << " pieces" << std::endl;
        for (const auto& str : lst) {
            std::cerr << "piece" << cnt++ << " containing " << str.size() << " chars" << std::endl;
            actual += str;
        }
    }

    return expected == actual ? EXIT_SUCCESS : EXIT_FAILURE;
}
